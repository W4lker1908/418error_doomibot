import telebot
import pymysql as mariadb
data_users = []
connection = mariadb.connect(user='root', password='1908mariapasswd*', database='data')
cursor = connection.cursor()
code = "752976559:AAFXMdAvm1CQ8OFcYTQDhEwuGZdXXC5X5lQ"
telebot.apihelper.proxy = {'https': 'socks5h://geek:socks@t.geekclass.ru:7777'}
bot = telebot.TeleBot(token=code)

@bot.message_handler(commands=["start"])        # функция для считывания команды /start
def echo_start(message):
    global data_users
    global user_name
    cursor.execute("SELECT name FROM users WHERE name=%s", (message.chat.username))

    if len(cursor.fetchall()) == 0: # случай если нет пользователя в базе данных
        cursor.execute("INSERT INTO users (name, skills) VALUES (%s, %s) ", (message.chat.username, "0"))
        connection.commit()
    user_name = message.chat.username
    cursor.execute("SELECT * FROM users")
    data_users = cursor.fetchall()

    bot.send_message(message.chat.id, "Привет, я DoomiBot, программа, помогающая в поиске людей, способных помочь вам с учёбой. Для вывода инструкции по командам используйте '/gotohelp'")
    
    
@bot.message_handler(commands=["gotohelp"])     # функция для считывания команды /gotohelp
def echo_help(message):
    bot.send_message(message.chat.id, "Краткое руководство:\n1) '/request' - начать поиск по возможной теме\n2) '/changeinf' - изменить загруженную информацию, автоматическое добавление аккаунта в журнал 'students'\n 3) '/showinf' - показать информацию аккаунта\nДа прибудет с вами skill!")


@bot.message_handler(commands=["request"])      # функция для считывания команды /request
def echo_request(message):
    m = bot.send_message(message.chat.id, "Направления и темы в них\n           Анализ Данных:\n1) 'Задачи и признаки машинного обучения'\n2) 'Линейная регрессия'\n3) 'Модули matplotlib, pandas, numpy'")
    bot.register_next_step_handler(message, check)

@bot.message_handler(commands=["changeinf"])       # функция для считывания команды /changeinf
def echo_gotochinf(message):
    bot.send_message(message.chat.id, "Направления и темы в них\n       Анализ Данных:\n 0)Нет тем\n1) 'Задачи и признаки машинного обучения'\n2) 'Линейная регрессия'\n3) 'Модули matplotlib, pandas, numpy'")
    bot.register_next_step_handler(message, myfunc)

def myfunc(message):
    print(message.text)
    cursor.execute("UPDATE users SET skills=%s WHERE name=%s", (message.text, message.chat.username))
    connection.commit()

@bot.message_handler(commands=["showinf"])      #функция для считывания команды /showinf
def echo_showinf(message):
    cursor.execute("SELECT skills FROM users WHERE name=%s", (message.chat.username))
    skill = cursor.fetchall()[0][0]
    bot.send_message(message.chat.id, "Ваш никнейм: {0}, ваши дополнительные данные: {1}".format(message.chat.username, skill))
    
def check(message):   
    global data_users
    cursor.execute("SELECT * FROM users")
    data_users = cursor.fetchall()
    if "1" in message.text:
        for i in data_users:
            if "1" in i[1]:
                bot.send_message(message.chat.id, "@" + i[0])
    elif "2" in message.text:
        for i in data_users:
            if "2" in i[1]:
                bot.send_message(message.chat.id, "@" + i[0])
    elif "3" in message.text:
        for i in data_users:
            if "3" in i[1]:
                bot.send_message(message.chat.id, "@" + i[0])
bot.polling(none_stop=True)
